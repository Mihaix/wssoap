<?php 
//Archivo para crear un WEB SERVICE sencillo en PHP
//Usamos la libreria nuSOAP

include('lib/nusoap.php');

$server = new soap_server();
$server->configureWSDL('servicionoticias','urn:servicionoticias');

$conexion=new mysqli('localhost','root','','symnoticias');
$conexion->set_charset('utf-8');

$server->register("getNoticia",
	array("idNoticia"=>"xsd:integer"),
	array("return"=>"xsd:string"),
	"urn:servicionoticias",
	"urn:servicionoticias#getNoticia",
	"rpc",
	"encoded",
	"Obtener la noticia a través de ID"
	);


$server->register("getContenido",
	array("idNoticia"=>"xsd:integer"),
	array("return"=>"xsd:string"),
	"urn:servicionoticias",
	"urn:servicionoticias#getContenido",
	"rpc",
	"encoded",
	"Obtener contenido de la noticia"
	);

function getNoticia($idNoticia){
	global $conexion;
	$sql="SELECT * FROM noticia WHERE id=$idNoticia";
	$consulta=$conexion->query($sql);
	$r=$consulta->fetch_array();
	return $r['titulo'];
}

function getContenido($idNoticia){
	global $conexion;	
	$sql="SELECT * FROM noticia WHERE id=$idNoticia";
	$consulta=$conexion->query($sql);
	$r=$consulta->fetch_array();
	return $r['contenido'];
}

//Devolvemos los datos que nos piden
$server->service(file_get_contents("php://input"));

$conexion->close();

?>